function sliceText(str, count){
	return str.slice(0, count);
}

function customSelect(){
	var list = $('.c-custom-select');

	if(list.length){
		$.each(list, function(){
			var elem = $(this),
				_placeholder;

			elem.data('placeholder') ? _placeholder = elem.data('placeholder') : _placeholder = elem.children().eq(0).text();

			$('.c-custom-select').select2({
				minimumResultsForSearch: Infinity,
				placeholder: _placeholder
			});

		});
	}
}

function rangeSlider(){
	var rangeSliders = $('.c-range-slider');

	if (rangeSliders.length){
		$.each(rangeSliders, function(){
			var elem = $(this),
				fromInput = elem.parent().find('.c-range-from'),
				toInput = elem.parent().find('.c-range-to');

			elem.slider({
				range: true,
				min: elem.data('min'),
				max: elem.data('max'),
				values: [ elem.data('val-min'), elem.data('val-max') ],
				slide: function( event, ui ) {
					var handle = elem.find('.ui-slider-handle');

					fromInput.val(ui.values[ 0 ] + 'р');
					fromInput.parent().css('left', handle.eq(0).position().left);
					toInput.val(ui.values[ 1 ] + 'р');
					toInput.parent().css('left', handle.eq(1).position().left);
				}
			});

			var handle = elem.find('.ui-slider-handle');
			fromInput.val(elem.slider( "values", 0 ) + 'р');
			fromInput.parent().css('left', handle.eq(0).position().left);
			toInput.val(elem.slider( "values", 1 ) + 'р');
			toInput.parent().css('left', handle.eq(1).position().left);
		});
	}
}

function counter(){
	var counterList = $('.c-counter');

	if (counterList.length){
		$.each(counterList, function(){
			var counter_elem = $(this),
				plus = counter_elem.find('.c-plus'),
				minus = counter_elem.find('.c-minus'),
				input = counter_elem.find('.c-count');

			plus.on('click', function(){
				var curr = input.val();

				curr = parseInt(curr) + 1;

				input.val(curr);
			});

			minus.on('click', function(){
				var curr = input.val();

				curr = parseInt(curr);

				if(curr !== 1){
					curr--;
				}

				input.val(curr);
			});
		});
	}
}

function bigImageSlide(){
	var bigImages = $('.c-big-image');

	if (bigImages.length){
		$.each(bigImages, function(){
			var elem = $(this),
				thumbs = elem.parent().find('.c-thumb');

			thumbs.on('click', function(){
				elem.find('img').attr('src', $(this).data('big-image'));
			});
		});
	}
}

function showPreloader(){
	$('.c-preloader, .c-layout').fadeOut('fast');
}

function hidePreloader(){
	$('.c-preloader, .c-layout').fadeIn('fast');
}

function removeGoodFromBascket(link){
	link.on('click', function(e){
		e.preventDefault();

		var wrap = link.closest('.c-bascket-line'),
			firstCell = wrap.find('.c-bascket-image'),
			children = wrap.children(),
			index = link.index(),
			sublineCount = link.parent().children().length,
			result;

		if (sublineCount > 1){
			result = children.not(firstCell);

			result.each(function(){
				$(this).children().eq(index).slideUp(200, function(){
					$(this).remove();
				});
			});
		}
		else{
			result = wrap;
			result.push(wrap.next('hr'));

			result.each(function(){
				$(this).slideUp(200, function(){
					var bascket = $(this).closest('.c-bascket');
					$(this).remove();
					if(bascket.children('.c-bascket-line').length == 0){
						bascket.html('<p>Ваша корзина пуста</p>')
					}
				});
			});
		}

		
	});
}


$(function(){
	$('.c-limit-text').each(function(){
		var elem = $(this),
			len = elem.data('length');

		if (elem.text().length > len){
			elem.text(sliceText(elem.text(), len)+'...');
		}
	});

	customSelect();
	rangeSlider();

	counter();
	bigImageSlide();

	$(document)
        .ajaxStart(function() {
            showPreloader();
        })
        .ajaxStop(function() {
            hidePreloader();
        })
        .ajaxError(function() {
        	hidePreloader();
        });

    $('body').on('click', '.c-slide-check', function(){
    	var elem = $(this);

    	if (elem.attr('type') == 'checkbox'){
    		elem.parent().toggleClass('active');
    	}
    	else{
    		elem.parent().addClass('active');
    		elem.parent().siblings().removeClass('active');
    	}
    })
    .on('click', '.c-cancel-order', function(e){
		e.preventDefault();

		$(this).closest('tr').remove();
    });

    $('.c-create-order').on('click', function(e){
    	e.preventDefault();

    	$('.c-bascket-detail').slideDown(200);
    });
	
    var bascketLinks = $('.c-remove-good');

    if (bascketLinks.length){
    	$.each(bascketLinks, function(){
    		removeGoodFromBascket($(this))
    	});
    }

    $('body').on('click', '.c-bascket-clear', function(e){
    	e.preventDefault();
    	var bascket = $(this).closest('.c-bascket');

    	bascket.empty();
		bascket.html('<p>Ваша корзина пуста</p>');
    })
    .on('click', '.c-activate-input', function(e){
		e.preventDefault();
		var input = $(this).parent().find('.c-disabled-input');

		input.removeAttr('disabled');
    });

    // form validation
    $.validate({
    	borderColorOnError : '#f2093a',
    });
});
var gulp = require('gulp'),
	autoprefixer = require('gulp-autoprefixer'),
	concat = require('gulp-concat'),
	less = require('gulp-less'),
	jade = require('gulp-jade'),
	browserSync = require('browser-sync'),
	reload = browserSync.reload,
	minifyCss = require('gulp-minify-css'),
	rename = require('gulp-rename'),
	imagemin = require('gulp-imagemin'),
	pngquant = require('imagemin-pngquant'),
	uglify = require('gulp-uglify');

var path = {
	html: {
		src:  ['./source/**/*.jade', '!./source/blocks/**/*.jade'],
		dest: './static'
	},
	css: {
		src: './source/less/all.less',
		dest: './static/css',
		watch: './source/less/**/*.less'
	},
	js: {
		src: './source/js/**/*.js',
		watch: 'source/js/**/*',
		dest: './static/js',
	},
	images: {
		src: './source/images/**/*.{jpg,jpeg,png,gif,svg}',
		watch: 'source/images/**/*',
		dest: './static/images/',
	}
}


gulp.task('browser-sync', function() {
    browserSync.init([
		'*.html',
		'css/*.css',
		'**/*.{png,jpg,svg}',
		'js/*.js',
		'fonts/*.{eot,woff,woff2,ttf}'
	], {
		open: true,
		server: { baseDir: './static' }
	});
});

gulp.task('jade', function(){
	gulp.src(path.html.src)
	.pipe(jade({
		basedir: './source',
		pretty: '\t'
	}))
	.pipe(gulp.dest(path.html.dest))
	.pipe(reload({stream:true}));
});

gulp.task('less', function(){
	gulp.src(path.css.src)
	.pipe(less())
	.pipe(autoprefixer({
		browsers: ['last 2 version', '> 2%', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'android 4'],
		cascade: false
	}))
	.pipe(gulp.dest(path.css.dest))
	.pipe(reload({stream:true}));
});

gulp.task('js', function(){
	gulp.src(path.js.src)
	.pipe(gulp.dest(path.js.dest))
	.pipe(reload({stream:true}));
});

gulp.task('copy', function(){
	gulp.src('./source/fonts/**/*{.woff,.eot,.woff2,.ttf}')
	.pipe(gulp.dest('./static/fonts'))
	gulp.src('./source/ajax/**/')
	.pipe(gulp.dest('./static/ajax'))
	gulp.src('./source/*.{png, ico, xml, json}')
	.pipe(gulp.dest('./static/'))
});

gulp.task('images', function () {
    gulp.src(path.images.src)
    .pipe(imagemin({
        progressive: true,
        svgoPlugins: [{removeViewBox: false}],
        use: [pngquant()]
    }))
    .pipe(gulp.dest(path.images.dest))
    .pipe(reload({stream:true}));
});

gulp.task('build', ['copy', 'jade', 'less', 'js', 'images']);

gulp.task('default', ['build', 'browser-sync'], function () {
    gulp.watch('source/**/*.jade', ['jade']);
    gulp.watch(path.css.watch, ['less']);
    gulp.watch(path.images.watch, ["images"]);
    gulp.watch(path.js.watch, ['js']);
});